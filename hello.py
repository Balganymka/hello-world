from flask import Flask, request, jsonify
from flask import Response
app = Flask(__name__)


@app.route("/", methods=['POST'])
def hello():
    data = {}
    x = int(request.form['a'])
    y = int(request.form['b'])
    data['result'] = x + y
    return jsonify(data)

    # return Response({"result": 5}, status=200, mimetype='application/json')


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
